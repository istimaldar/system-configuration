#!/usr/bin/env bash

if [[ ! $(type -P task) ]];
then

    mkdir -p .bin
    cd .bin
    if [[ ! -f task ]];
    then
        curl -SLO https://github.com/go-task/task/releases/download/v3.23.0/task_linux_amd64.tar.gz
        tar -zxvf task_linux_amd64.tar.gz
    fi
    export TASK_BINARY="$(readlink -f .)/task"
else
    export TASK_BINARY="$(type -P task)"
fi

$TASK_BINARY
