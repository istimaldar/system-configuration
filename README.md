# Nix OS and system manager system settings

This repo will help you to set up a workspace and development Kubernetes cluster with a single command.

## Prerequirements

Before creating the infrastructure, you should create a `.secrets.json` file. To get reference on required fields and how to obtain values for them, you should add a `$schema` field to the file:
```json
{
    "$schema": "./schemas/secrets.schema.json"
}
```

After you've created this file, you can run this command to start deployment: 

```bash
./bootstrap.sh
```