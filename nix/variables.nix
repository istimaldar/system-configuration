{ ... }: {
  DOCKER_HOST = "unix:///run/user/1000/podman/podman.sock";
  DOCKER_SOCK = "/run/user/1000/podman/podman.sock";
  PATH = "$HOME/.nix-profile/bin:$PATH";
  XDG_DATA_DIRS = "$HOME/.nix-profile/share:$XDG_DATA_DIRS";
  LIBVA_DRIVER_NAME = "iHD";
  QT_XCB_GL_INTEGRATION = "none";
}