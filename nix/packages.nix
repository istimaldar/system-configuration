{ pkgs, ... }: with pkgs; [
  nerdfonts
  megasync
  keepassxc
  devbox
  powershell
  google-cloud-sdk
  yq-go
  jq
  jdk11
  maven
  krita
  insomnia
  inxi
  maim
  xclip
  d2
  languagetool

  podman
  podman-compose
  kube3d
  kubectl
  doctl
  sops
  go-task
  terraform
  tilt
  kubernetes-helm
  helmsman

  slack
  telegram-desktop
  skypeforlinux
  discord
  spotify
  firefox

  jetbrains.idea-ultimate
  jetbrains.goland
  jetbrains.datagrip
  jetbrains.rider
  jetbrains.pycharm-professional

  logseq
  anki

  httpie
  ncdu
  duf
  tldr

  (python311.withPackages (ps: with ps; [
    pip
    virtualenv
  ]))
]
