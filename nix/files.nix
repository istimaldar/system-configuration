{ ... }: {
  ".gnupg/gpg-agent.conf".source = ./files/gpg-agent.conf;
  ".p10k.zsh".source = ./files/.p10k.zsh;
}