{ config, pkgs, ... }: {
  home = {
    username = "istimaldar";
    homeDirectory = "/home/istimaldar";
    stateVersion = "22.11";

    packages = import ./nix/packages.nix pkgs;
    file = import ./nix/files.nix pkgs;
    sessionVariables = import ./nix/variables.nix pkgs;
    shellAliases = import ./nix/aliases.nix pkgs;
  };

  programs = import ./nix/programs.nix pkgs;
  services = import ./nix/services.nix pkgs;

  fonts.fontconfig.enable = true;
}